GeneFlow App for Creating Reference Index and Dict
==================================================

Version: 0.1

This GeneFlow app creates a reference index and dict file using samtools and Picard CreateSequenceDictionary.

Inputs
------

1. reference_sequence: Reference sequence FASTA file.

Parameters
----------

1. output: Output directory - The name of the output directory to place the fasta, fai, and dict files. Default: output.

